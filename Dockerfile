FROM node:alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . .
RUN npm install
RUN npm install react-scripts@next -g
RUN npm run build
RUN npm install -g serve

CMD serve -l 3201 build

LABEL name=rt-frontend
