import { connect } from 'react-redux';
import { getData } from '../redux/modules/tableData';
import LoadButton from '../components/BodyHeader/LoadButton';
import { getExportLink } from '../redux/modules/moduleExport';
import { getRequestData } from '../redux/selectors/requestData';

const mapStateToProps = state => ({
  data: state.tableData.data,
  params: getRequestData(state),
});

const mapDispatchToProps = {
  getData,
  getExportLink,
};

const LoadButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoadButton);

export default LoadButtonContainer;
