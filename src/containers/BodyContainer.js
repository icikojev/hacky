import { connect } from 'react-redux';
import { getData, updateHiddenColumnNames, updateCurrentTable } from '../redux/modules/tableData';
import Body from '../components/Body/Body';
import { getTableData } from '../redux/selectors/tableData';
import { getKpiData } from '../redux/selectors/kpiData';

const mapStateToProps = state => ({
  tableData: getTableData(state),
  kpiData: getKpiData(state),
  currentTable: state.tableData.currentTable,
  client: state.clients.selectedClient,
  network: state.clients.network,
  others: state.clients.others,
  hiddenColumnNames: state.tableData.hiddenColumnNames,
  errorMessage: state.tableData.errorMessage,
});

const mapDispatchToProps = {
  getData,
  updateHiddenColumnNames,
  updateCurrentTable,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Body);
