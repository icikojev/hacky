import { connect } from 'react-redux';
import { getExportLink } from '../redux/modules/moduleExport';
import ExportButton from '../components/Body/ExportButton';

const mapStateToProps = state => ({
  exportLink: state.moduleExport.exportLink,
  openModal: state.moduleExport.openModal,
  exportData: {
    main: state.tableData.main,
    compare: state.tableData.compare,
    breakdown: state.tableData.breakdown,
    meta: state.tableData.meta,
    clientId: state.clients.selectedClient.client_unique_id,
  },
});

const mapDispatchToProps = {
  getExportLink,
};

const ExportButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExportButton);

export default ExportButtonContainer;
