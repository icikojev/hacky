import { connect } from 'react-redux';
import { getClients, updateSelectedClient, updateNetwork } from '../redux/modules/clients';
import SelectClient from '../components/AppBar/SelectClient';

const mapStateToProps = state => ({
  clients: state.clients.clients,
});

const mapDispatchToProps = {
  getClients,
  updateSelectedClient,
  updateNetwork,
};

const SelectClientContainer = connect(mapStateToProps, mapDispatchToProps)(SelectClient);

export default SelectClientContainer;
