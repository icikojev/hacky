import { connect } from 'react-redux';
import {
  updateReportType,
  updateStartEndDate,
  updateStartEndDateCompare,
  numOfWeeks,
} from '../redux/modules/moduleReportPicker';
import PickerReportType from '../components/BodyHeader/PickerReportType';

const mapStateToProps = state => ({
  reportType: state.moduleReportPicker.reportType,
  rangeNumber: state.moduleReportPicker.rangeNumber,
});

const mapDispatchToProps = {
  updateReportType,
  updateStartEndDate,
  updateStartEndDateCompare,
  numOfWeeks,
};

const PickerReportContainer = connect(mapStateToProps, mapDispatchToProps)(PickerReportType);

export default PickerReportContainer;
