import { connect } from 'react-redux';
import { getData } from '../redux/modules/moduleData';
import CenterComponent from '../components/Hack/CenterComponent';

const mapStateToProps = state => ({
  data: state.moduleData.data,
});

const mapDispatchToProps = {
  getData,
};

const ExportButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CenterComponent);

export default ExportButtonContainer;
