import api from '../../helpers/api';

const actionPrefix = 'peribianreporting/moduleData/';

export const GET_DATA_START = `${actionPrefix}GET_DATA_START`;
export const GET_DATA_SUCCESS = `${actionPrefix}GET_DATA_SUCCESS`;
export const GET_DATA_FAILURE = `${actionPrefix}GET_DATA_FAILURE`;

const initialState = {
  data: {},
  gettingData: false,
  errorMessage: false,
};

export const getData = () => dispatch => {
  dispatch({ type: GET_DATA_START });

  return api
    .get('/questions')
    .then(response => dispatch({ type: GET_DATA_SUCCESS, payload: response.data }))
    .catch(error => dispatch({ type: GET_DATA_FAILURE, payload: error }));
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA_START:
      return {
        ...state,
        data: false,
        gettingData: true,
      };
    case GET_DATA_SUCCESS:
      return {
        ...state,
        gettingData: false,
        errorMessage: false,
        data: action.payload,
      };
    case GET_DATA_FAILURE:
      return {
        ...state,
        gettingData: false,
        errorMessage: 'Sorry, there is no export link available at the moment!',
      };
    default:
      return state;
  }
};

export default reducer;
