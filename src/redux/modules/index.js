import { combineReducers } from 'redux';
import moduleReportPicker from './moduleReportPicker';
import tableData from './tableData';
import clients from './clients';
import moduleAuth from './moduleAuth';
import moduleData from './moduleData';

export default combineReducers({
  moduleReportPicker,
  tableData,
  clients,
  moduleAuth,
  moduleData,
});
