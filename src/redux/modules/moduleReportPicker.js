import moment from 'moment';

const actionPrefix = 'peribianreporting/moduleReportPicker/';

export const REPORT_TYPE_CHANGE = `${actionPrefix}REPORT_TYPE_CHANGE`;
export const DATE_CHANGE = `${actionPrefix}DATE_CHANGE`;
export const DATE_CHANGE_COMPARE = `${actionPrefix}DATE_CHANGE_COMPARE`;
export const NUM_OF_WEEKS = `${actionPrefix}NUM_OF_WEEKS`;

const initialState = {
  reportType: 'week',
  startingDate: moment(),
  endingDate: moment(),
  compareFrom: moment().subtract(1, 'week'),
  compareTo: moment().subtract(1, 'week'),
  rangeNumber: '',
};

export const updateReportType = reportType => dispatch =>
  dispatch({
    type: REPORT_TYPE_CHANGE,
    reportType,
  });

export const updateStartEndDate = (startingDate, endingDate) => dispatch =>
  dispatch({
    type: DATE_CHANGE,
    startingDate,
    endingDate,
  });

export const updateStartEndDateCompare = (compareFrom, compareTo) => dispatch =>
  dispatch({
    type: DATE_CHANGE_COMPARE,
    compareFrom,
    compareTo,
  });

export const numOfWeeks = rangeNumber => dispatch =>
  dispatch({
    type: NUM_OF_WEEKS,
    rangeNumber,
  });

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REPORT_TYPE_CHANGE:
      return {
        ...state,
        reportType: action.reportType,
      };
    case DATE_CHANGE:
      return {
        ...state,
        startingDate: action.startingDate,
        endingDate: action.endingDate,
      };
    case DATE_CHANGE_COMPARE:
      return {
        ...state,
        compareFrom: action.compareFrom,
        compareTo: action.compareTo,
      };
    case NUM_OF_WEEKS:
      return {
        ...state,
        rangeNumber: action.rangeNumber,
      };
    default:
      return state;
  }
};

export default reducer;
