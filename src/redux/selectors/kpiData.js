import { createSelector } from 'reselect';
import _isEmpty from 'lodash/isEmpty';
import _forEach from 'lodash/forEach';

const prepareKpiData = ({ tableData }) => {
  if (_isEmpty(tableData.main)) {
    return [];
  }

  let kpi = 0;
  _forEach(tableData.main, item => {
    const keys = Object.keys(item);

    const regex = new RegExp(`clicks`, 'ig');
    const matchedMetrics = keys.filter(i => i.match(regex));
    // if (kpi[matchedMetrics[0]]) {
    //   kpi[matchedMetrics[0]] += item[matchedMetrics[0]];
    // } else {
    //   kpi[matchedMetrics[0]] = item[matchedMetrics[0]];
    // }
    kpi += item[matchedMetrics[0]];
  });

  return [
    {
      metric: kpi,
      value: 100,
      better: '>',
      kpiName: 'Clicks',
    },
  ];
};

export const getKpiData = createSelector(prepareKpiData, kpiData => kpiData);
