import { createSelector } from 'reselect';
import columnNames from '../../helpers/columnNames';
import _isEmpty from 'lodash/isEmpty';
import _forEach from 'lodash/forEach';
import _uniq from 'lodash/uniq';
import _omit from 'lodash/omit';
import capitalCase from 'to-capital-case';
import countryFlags from '../../helpers/emojiFlags';

const prepareTables = ({ main, compare }, { period, allFilter }, breakdown) => {
  const keyNames = Object.keys(main);

  let metrics = [];
  _forEach(main, item => {
    metrics = metrics.concat(Object.keys(item));
  });

  const periodTitle = columnNames[period] ? columnNames[period].title : capitalCase(period);

  let periodColumns = [];
  let periodRows = [];
  let periodColumnExtensions = [];

  if (allFilter || breakdown) {
    const periodTItle = allFilter || breakdown;
    periodColumns = [
      { name: 'period', title: periodTitle },
      { name: 'allFilter', title: capitalCase(periodTItle) },
    ];
    periodColumnExtensions = [
      { columnName: 'period', align: 'center', width: 100 },
      { columnName: 'allFilter', align: 'center', width: 200 },
    ];
    keyNames.forEach(item => {
      const [weekOrMonth, ...filter] = item.split('-');
      let allFilterName;
      if (periodTItle === 'country') {
        const [countryCode, ...rest] = filter;
        const country = countryFlags(countryCode);
        allFilterName = country
          ? `${country.emoji} ${country.name} ${capitalCase(rest.join(' '))}`
          : capitalCase(filter.join(' '));
      } else {
        allFilterName = capitalCase(filter.join(' '));
      }
      periodRows.push({ period: weekOrMonth, allFilter: allFilterName });
    });
  } else {
    periodColumns = [{ name: 'period', title: periodTitle }];
    keyNames.forEach(item => periodRows.push({ period: item }));
    periodColumnExtensions = [{ columnName: 'period', align: 'center', width: 200 }];
  }

  const tableData = {
    period: {
      rows: periodRows,
      columns: periodColumns,
      columnExtensions: periodColumnExtensions,
    },
    totalLabel: {
      rows: [{ label: 'Total' }],
      columns: [{ name: 'label', title: '' }],
      columnExtensions: [
        { columnName: 'label', align: 'center', width: allFilter || breakdown ? 300 : 200 },
      ],
    },
    regular: [],
    total: [],
  };

  _uniq(metrics).forEach(metric => {
    const columns = [
      { name: `${metric}_main`, title: 'Main' },
      { name: `${metric}_compare`, title: 'Compare' },
      { name: `${metric}_percentage`, title: '%' },
    ];

    const totalColumns = [
      { name: `${metric}_main_total`, title: 'Main' },
      { name: `${metric}_compare_total`, title: 'Compare' },
      { name: `${metric}_percentage_total`, title: '%' },
    ];

    const tableColumnExtensions = [
      { columnName: `${metric}_main`, align: 'center', width: 200 },
      { columnName: `${metric}_compare`, align: 'center', width: 200 },
      { columnName: `${metric}_percentage`, align: 'center', width: 200 },
    ];

    const tableColumnExtensionsTotal = [
      { columnName: `${metric}_main_total`, align: 'center', width: 200 },
      { columnName: `${metric}_compare_total`, align: 'center', width: 200 },
      { columnName: `${metric}_percentage_total`, align: 'center', width: 200 },
    ];

    const tableColumnVisibilityColumnExtensions = [
      { columnName: `${metric}_main`, togglingEnabled: false },
    ];

    const tableColumnVisibilityColumnExtensionsTotal = [
      { columnName: `${metric}_main_total`, togglingEnabled: false },
    ];

    const topTitle = columnNames[metric]
      ? columnNames[metric].title
      : metric
          .split(/(?=[A-Z])/)
          .slice(1)
          .join(' '); // fbUpperCase => Upper Case
    const rows = [];
    const totalRows = [];

    const mainValues = Object.values(main);
    const compareValues = Object.values(compare);

    let row = {};
    let totalRow = {
      id: `total`,
      [`${metric}_main_total`]: 0,
      [`${metric}_compare_total`]: 0,
      [`${metric}_percentage_total`]: 'N/A',
    };

    for (let c = 0; c < keyNames.length; c++) {
      row[c] = {};
      row[c][`${metric}_main`] = mainValues[c][metric] ? +mainValues[c][metric].toFixed(2) : 'N/A';
      totalRow[`${metric}_main_total`] = mainValues[c][metric] || 0;
      totalRow[`${metric}_compare_total`] = compareValues[c][metric] || 0;

      if (compareValues[c] && !_isEmpty(compareValues[c])) {
        row[c][`${metric}_compare`] = compareValues[c][metric]
          ? +compareValues[c][metric].toFixed(2)
          : 'N/A';
        // totalRow[`${metric}_compare_total`] = compareValues[c][metric] || 0;
        if (compareValues[c][metric] && mainValues[c][metric] !== 0) {
          row[c][`${metric}_percentage`] =
            +(
              ((mainValues[c][metric] - compareValues[c][metric]) / compareValues[c][metric]) *
              100
            ).toFixed(2) + '%';
        } else {
          row[c][`${metric}_percentage`] = 'N/A';
        }
      } else {
        row[c][`${metric}_compare`] = 'N/A';
        row[c][`${metric}_percentage`] = 'N/A';
      }
      totalRow[`${metric}_percentage_total`] = row[c][`${metric}_percentage`] || 0;
      row[c].id = `${metric}-${c}`;
      rows.push(row[c]);
    }

    totalRow.id = `${metric}-total-row`;
    totalRow[`${metric}_main_total`] = +totalRow[`${metric}_main_total`].toFixed(2);
    totalRow[`${metric}_compare_total`] =
      totalRow[`${metric}_compare_total`] !== 'N/A'
        ? +totalRow[`${metric}_compare_total`].toFixed(2)
        : 'N/A';

    totalRow[`${metric}_percentage_total`] = totalRow[`${metric}_percentage_total`];
    // totalRow[`${metric}_main_total`] !== 0 && totalRow[`${metric}_compare_total`] !== 'N/A'
    //   ? +(
    //       ((totalRow[`${metric}_main_total`] - totalRow[`${metric}_compare_total`]) /
    //         totalRow[`${metric}_compare_total`]) *
    //       100
    //     ).toFixed(2) + '%'
    //   : 'N/A';

    totalRows.push(totalRow);

    tableData.regular.push({
      metric,
      title: topTitle,
      rows,
      columns,
      columnExtensions: tableColumnExtensions,
      visibilityColumnExtensions: tableColumnVisibilityColumnExtensions,
    });
    tableData.total.push({
      metric,
      title: null,
      rows: totalRows,
      columns: totalColumns,
      columnExtensions: tableColumnExtensionsTotal,
      visibilityColumnExtensions: tableColumnVisibilityColumnExtensionsTotal,
    });
  });
  const periodRowsLength = tableData.period['rows'].length - 1;
  const temporaryPeriod = _omit(tableData.period['rows'], [periodRowsLength]);
  tableData.period.rows = Object.values(temporaryPeriod);

  _forEach(tableData.regular, (item, key) => {
    const rowsLength = tableData.regular[key]['rows'].length - 1;
    const temporaryRegular = _omit(tableData.regular[key]['rows'], [rowsLength]);
    tableData.regular[key]['rows'] = Object.values(temporaryRegular);
  });

  return tableData;
};

const prepareAllTableData = ({ tableData }) => {
  if (_isEmpty(tableData.main)) {
    return {};
  }

  const all = prepareTables(tableData, tableData.meta);
  const breakdown = {};
  _forEach(tableData.breakdown, (data, key) => {
    breakdown[key] = prepareTables(data, tableData.meta, key);
  });

  return {
    all,
    ...breakdown,
  };
};

export const getTableData = createSelector(prepareAllTableData, tableData => tableData);
