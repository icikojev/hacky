import { createSelector } from 'reselect';
import _isEmpty from 'lodash/isEmpty';

export const getRequestData = createSelector(({ clients, moduleReportPicker }) => {
  if (_isEmpty(clients.network)) {
    return {};
  }

  const dimensions = clients.others
    ? clients.selectedClient.networks.others[clients.network].dimensions
    : clients.selectedClient.networks[clients.network].dimensions;

  return {
    clientId: clients.selectedClient.client_unique_id,
    network: clients.network,
    others: clients.others,
    dimensions,
    filters: clients.selectedFilters,
    mainDateFrom: moduleReportPicker.startingDate.clone().format(),
    mainDateTo: moduleReportPicker.endingDate.clone().format(),
    compareDateFrom: moduleReportPicker.compareFrom.clone().format(),
    compareDateTo: moduleReportPicker.compareTo.clone().format(),
    period: moduleReportPicker.reportType,
  };
}, requestData => requestData);
