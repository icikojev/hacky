import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Ads from './Screen1/Ads';
import Answers from './Screen1/Answers';
import AnswersPercent from './Screen1/AnswersPercent';
import GameInfo from './Screen1/GameInfo';
import UserInfo from './Screen1/UserInfo';
import Questions from './Screen1/Questions';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '40vw',
    background: 'white',
  },
});

class CenterComponent extends React.Component {
  render() {
    const { classes } = this.props;
    console.log('this.props', this.props);
    return (
      <div className={classes.root}>
        <Ads />
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            background: '#297C46',
            height: '88vh',
            width: '42vw',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              background: 'white',
              height: '82vh',
              width: '40vw',
              marginTop: '26px',
              marginLeft: '18px',
              borderRadius: '20px',
            }}
          >
            <UserInfo />
            <GameInfo />
            <Questions />
            <Answers data={this.props} />
            {/* <AnswersPercent /> */}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
