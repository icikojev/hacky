import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '12vh',
    width: '42vw',
    // borderRadius: '20px',
    // border: '1px solid black',
  },
});

class CenterComponent extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div style={{ borderRadius: '20px' }}>
          <img
            src="bet365.png"
            alt="Girl in a jacket"
            width="100%"
            height="120px"
            borderRadius="20px"
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
