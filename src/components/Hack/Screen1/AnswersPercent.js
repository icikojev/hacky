import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '33vh',
    // border: '1px solid black',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class CenterComponent extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      color1: '#8CF293',
      color2: '#8CF293',
      jedan: 'jedan',
    };
  }

  render() {
    const { classes } = this.props;
    const { color1, color2, jedan } = this.state;
    return (
      <div className={classes.root}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Button
            size="large"
            className={classes.button}
            variant="raised"
            style={{
              borderRadius: '20px',
              width: '38vw',
              height: '60px',
              fontSize: '20px',
              textTransform: 'capitalize',
              background: '#E1E6E5',
              color: 'black',
              border: '1px solid grey',
              padding: 0,
              marginLeft: '20px',
              marginRight: '0px',
            }}
            onClick={this.onClickButton1}
          >
            <div
              style={{
                width: '30%',
                height: '-webkit-fill-available',
                paddingTop: '15px',
                background: color1,
                borderRadius: '20px',
                fontFamily: 'cursive',
                textAlign: 'start',
                paddingLeft: '15px',
              }}
            >
              30000 correct answers
            </div>
            <div
              style={{
                width: '70%',
                height: '-webkit-fill-available',
                paddingTop: '15px',
              }}
            />
          </Button>
          <Button
            size="large"
            className={classes.button}
            variant="raised"
            style={{
              borderRadius: '20px',
              width: '38vw',
              height: '60px',
              fontSize: '20px',
              textTransform: 'capitalize',
              background: '#E1E6E5',
              color: 'black',
              border: '1px solid grey',
              padding: 0,
              marginLeft: '20px',
              marginRight: '0px',
            }}
            onClick={this.onClickButton1}
          >
            <div
              style={{
                width: '70%',
                height: '-webkit-fill-available',
                paddingTop: '15px',
                background: color2,
                borderRadius: '20px',
                fontFamily: 'cursive',
                textAlign: 'start',
                paddingLeft: '15px',
              }}
            >
              70000 wrong answers
            </div>
            <div
              style={{
                width: '30%',
                height: '-webkit-fill-available',
                paddingTop: '15px',
              }}
            />
          </Button>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
