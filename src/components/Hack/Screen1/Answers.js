import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '33vh',
    // border: '1px solid black',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class CenterComponent extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      color1: '#E1E6E5',
      color2: '#E1E6E5',
      jedan: 'jedan',
      data: false,
    };
  }

  onClickButton1 = () => {
    // this.props.data.getData();
    this.setState({ color1: 'green', data: this.props.data });
  };
  onClickButton2 = () => {
    this.setState({ color2: 'green', jedan: 'dva' });
  };

  render() {
    const { classes } = this.props;
    const { color1, color2, jedan } = this.state;
    console.log('this.state', this.state);
    return (
      <div className={classes.root}>
        <div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Button
              size="large"
              className={classes.button}
              variant="raised"
              style={{
                borderRadius: '20px',
                width: '38vw',
                height: '60px',
                fontSize: '20px',
                textTransform: 'capitalize',
                background: color1,
                color: 'black',
                border: '1px solid grey',
                padding: 0,
                marginLeft: '15px',
                marginRight: '0px',
              }}
              onClick={this.onClickButton1}
            >
              More than 10
            </Button>
            <Button
              size="large"
              className={classes.button}
              style={{
                borderRadius: '20px',
                width: '38vw',
                height: '60px',
                fontSize: '20px',
                textTransform: 'capitalize',
                background: color2,
                color: 'black',
                border: '1px solid grey',
                padding: 0,
                marginLeft: '15px',
                marginRight: '0px',
              }}
              onClick={this.onClickButton2}
            >
              More than 10
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
