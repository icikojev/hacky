import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ProgressMobileStepper from './HelperComponents/ProgressMobileStepper';
import { heart } from 'react-icons-kit/icomoon/heart';
import { heartBroken } from 'react-icons-kit/icomoon/heartBroken';
import { Line, Circle } from 'rc-progress/lib';
import { users } from 'react-icons-kit/icomoon/users';
import { Icon } from 'react-icons-kit';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '10vh',
    width: '38vw',
    marginLeft: '20px',
    // border: '1px solid black',
  },
});

class CenterComponent extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
          }}
        >
          <Icon icon={users} size={32} style={{ color: 'grey' }} />
          <div
            style={{
              fontSize: '20px',
              color: 'grey',
            }}
          >
            100 000
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div style={{ fontSize: '22px' }}>Question 7/15</div>
          <Line
            style={{ width: '250px', height: '10px', marginTop: '10px' }}
            percent="50"
            strokeWidth="4"
            strokeColor="blue"
          />
          {/* <ProgressMobileStepper style={{}} /> */}
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}
        >
          <Icon icon={heartBroken} size={32} style={{ color: 'grey' }} />
          <Icon icon={heart} size={32} style={{ color: 'red' }} />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
