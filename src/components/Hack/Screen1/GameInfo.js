import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ReactCountryFlag from 'react-country-flag';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '20vh',
    // border: '1px solid black',
  },
});

class CenterComponent extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            border: '1px solid grey',
            borderRadius: '20px',
          }}
        >
          <div
            style={{
              fontSize: '26px',
              //   border: '1px solid grey',
              borderRadius: '20px',
              background: '#E1E6E5',
            }}
          >
            &nbsp;&nbsp;&nbsp;Croatia<ReactCountryFlag code="HR" />&nbsp;3&nbsp;:&nbsp;1&nbsp;<ReactCountryFlag code="RU" />
            Russia&nbsp;&nbsp;&nbsp;&nbsp;
          </div>
          <div
            style={{
              fontSize: '26px',
              //   borderRadius: '20px',
              //   borderRight: '1px solid black',
              paddingTop: '2px',
            }}
          >
            &nbsp;&nbsp;32:33&nbsp;&nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
