import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '25vh',
    // border: '1px solid black',
  },
});

class CenterComponent extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {' '}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <div
            style={{
              fontSize: '28px',
              textAlign: 'center',
              marginLeft: '20px',
              marginRight: '20px',
              fontFamily: 'cursive',
            }}
          >
            How many duels with team A win between minute 35 and minute 40?
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CenterComponent);
