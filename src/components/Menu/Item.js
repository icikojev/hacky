import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Star } from '@material-ui/icons';
import Icon from 'react-icons-kit';
import {
  facebook2 as facebook,
  google2 as adwords,
  bold as bing,
  yahoo as yandex,
} from 'react-icons-kit/icomoon';
import capitalCase from 'to-capital-case';

const icons = { adwords, facebook, Star, yandex, bing };

const Item = ({ item, selectedItem, onClick }) => {
  return (
    <ListItem
      key={item}
      button
      onClick={onClick(item, false)}
      divider
      value={item}
      style={item === selectedItem ? { backgroundColor: '#B0BEC5' } : {}}
    >
      {icons[item] ? <Icon icon={icons[item]} /> : <Star />}
      <ListItemText inset primary={capitalCase(item)} value={item} />
    </ListItem>
  );
};

export default Item;
