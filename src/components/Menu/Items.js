import React from 'react';
import Item from './Item';

const Items = ({ items, selectedItem, onClick }) => {
  return items.map(item => (
    <Item key={item} item={item} selectedItem={selectedItem} onClick={onClick} />
  ));
};

export default Items;
