import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Cookie from 'js-cookie';

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.secondary.contrastText,
    flexGrow: 1,
    display: 'block',
    height: 40,
  },
  title: {
    cursor: 'pointer',
  },
});

class AppTitle extends React.Component {
  state = {
    value: 0,
  };

  handleChange = event => {
    return window.location.reload();
  };

  logout = () => {
    Cookie.remove('jwt_token');
    window.location.reload();
  };

  render() {
    const { classes } = this.props;

    return (
      <AppBar position="fixed" className={classes.root}>
        <Toolbar style={{ minHeight: '40px' }}>
          <Typography
            onClick={this.handleChange}
            variant="title"
            color="inherit"
            className={classes.title}
            to="/"
          >
            Peribian reporting
          </Typography>
          {this.props.loggedIn && (
            <Button
              onClick={this.logout}
              style={{
                textTransform: 'capitalize',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginLeft: 'auto',
              }}
              color="inherit"
            >
              Sign out
              <ExitToApp style={{ marginLeft: '5px' }} />
            </Button>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

AppTitle.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppTitle);
