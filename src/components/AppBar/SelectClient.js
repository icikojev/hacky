import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import _find from 'lodash/find';

const styles = theme => ({
  formControl: {
    marginTop: 6,
    minWidth: 180,
  },
});

class SelectClient extends React.Component {
  state = {
    clientSelected: '',
    open: false,
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
    const client = _find(this.props.clients, { client_unique_id: event.target.value });
    this.props.updateSelectedClient(client);
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = event => {
    this.setState({ open: true });
    if (this.props.clients.length === 0) this.props.getClients();
  };

  render() {
    const { classes } = this.props;
    return (
      <form autoComplete="off">
        <FormControl className={classes.formControl}>
          <InputLabel style={{ marginBottom: '0px' }} htmlFor="controlled-open-select">
            Choose client
          </InputLabel>
          <Select
            style={{ textAlign: 'start' }}
            open={this.state.open}
            onClose={this.handleClose}
            onOpen={this.handleOpen}
            value={this.state.clientSelected}
            onChange={this.handleChange}
            inputProps={{
              name: 'clientSelected',
              id: 'controlled-open-select',
            }}
          >
            {this.props.clients
              ? this.props.clients.map(client => (
                  <MenuItem key={client.client_unique_id} value={client.client_unique_id}>
                    {client.name}
                  </MenuItem>
                ))
              : null}
          </Select>
        </FormControl>
      </form>
    );
  }
}

SelectClient.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SelectClient);
