import React from 'react';
import '../styles.css';
import { connect } from 'react-redux';
import CenterComponent from './Hack/CenterComponent';

class App extends React.Component {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          background: '#297C46',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}
        />
        <CenterComponent />
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            background: '#ccc',
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.moduleAuth,
  network: state.clients.network,
});

export default connect(mapStateToProps)(App);
