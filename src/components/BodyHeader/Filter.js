import React, { Component } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import capitalCase from 'to-capital-case';
import countryFlags from '../../helpers/emojiFlags';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      open: false,
    };
  }

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.props.onChange(event, this.props.level);
  };

  setLabel = (filter, chosenFilter) => {
    let label = capitalCase(filter);

    if (chosenFilter === 'country') {
      const country = countryFlags(filter);
      label = country ? `${country.emoji} ${country.name}` : capitalCase(filter);
    }

    return label;
  };

  render() {
    const { id, chosenFilter, filters, classes } = this.props;
    return (
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="controlled-open-select">Filter</InputLabel>
        <Select
          id={id}
          open={this.open}
          onClose={this.handleClose}
          onOpen={this.handleOpen}
          value={this.state.value}
          onChange={this.handleChange}
          inputProps={{
            name: 'value',
            id: 'controlled-open-select',
          }}
        >
          {filters.map((filter, key) => (
            <MenuItem key={key + 1} value={filter}>
              {this.setLabel(filter, chosenFilter)}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }
}

export default Filter;
