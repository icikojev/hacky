import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import DatetimeRangePicker from 'react-bootstrap-datetimerangepicker';
import moment from 'moment';
import 'bootstrap/dist/css/bootstrap.css';
import 'daterangepicker/daterangepicker.css';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import {
  updateReportType,
  updateStartEndDate,
  updateStartEndDateCompare,
  numOfWeeks,
} from '../../redux/modules/moduleReportPicker';

const styles = theme => ({
  root: {},
});

class PickerDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDropdowns: true,
      showWeekNumbers: true,
      locale: {
        format: 'MM/DD/YYYY',
        separator: ' - ',
        applyLabel: 'Apply',
        cancelLabel: 'Cancel',
        fromLabel: 'From',
        toLabel: 'To',
        customRangeLabel: 'Custom',
        weekLabel: 'W',
        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        monthNames: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ],
        firstDay: 1,
      },
      pickedRange: 'period',
      rangeValue: 0,
      startDate: moment()
        .subtract(1, 'week')
        .startOf('isoWeek'),
      endDate: moment()
        .subtract(1, 'week')
        .endOf('isoWeek'),
      maxDate: moment()
        .subtract(1, 'week')
        .endOf('isoWeek'),
      opens: 'right',
      alwaysShowCalendars: 'true',
      ranges: {
        'Current Week': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
        'Current Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Week': [
          moment()
            .subtract(1, 'week')
            .startOf('isoWeek'),
          moment()
            .subtract(1, 'week')
            .endOf('isoWeek'),
        ],
        'Last Month': [
          moment()
            .subtract(1, 'month')
            .startOf('month'),
          moment()
            .subtract(1, 'month')
            .endOf('month'),
        ],
      },
    };

    this.handleEvent = this.handleEvent.bind(this);
  }

  handleEvent(event, picker) {
    const { reportType, updateStartEndDate, updateStartEndDateCompare, numOfWeeks } = this.props;

    this.setState({
      startDate: picker.startDate,
      endDate: picker.endDate,
    });

    const rangeNumber =
      reportType === 'week'
        ? picker.endDate.diff(picker.startDate, 'weeks')
        : picker.endDate.diff(picker.startDate, 'months');

    let compareStart;
    let compareEnd;

    if (reportType === 'week') {
      compareStart = picker.startDate
        .clone()
        .subtract(1, 'week')
        .startOf('isoWeek');
      compareEnd = picker.endDate
        .clone()
        .subtract(1, 'week')
        .endOf('isoWeek');
    } else {
      compareStart = picker.startDate
        .clone()
        .subtract(1, 'month')
        .startOf('month');
      compareEnd = picker.endDate
        .clone()
        .subtract(1, 'month')
        .endOf('month');
    }
    numOfWeeks(rangeNumber);
    updateStartEndDateCompare(compareStart.clone(), compareEnd.clone());
    updateStartEndDate(picker.startDate.clone(), picker.endDate.clone());
  }

  render() {
    const start = this.state.startDate.format('ll');
    const end = this.state.endDate.format('ll');
    const label = start === end ? start : start + ' - ' + end;
    const buttonStyle = { width: '100%', minWidth: '240px' };
    const { reportType } = this.props;

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          margin: '24px 0px 0px 10px',
          width: '450px',
        }}
        className="form-group"
      >
        <label
          style={{
            marginRight: '4px',
            width: '50px',
            textAlign: 'center',
            padding: '0px',
          }}
          className="control-label col-md-3"
        >
          Choose {reportType ? reportType : 'period'}:
        </label>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            padding: '0px',
            width: '240px',
          }}
          className="col-md-4"
        >
          <DatetimeRangePicker
            alwaysShowCalendars={this.state.alwaysShowCalendars}
            opens={this.state.opens}
            showDropdowns={this.state.showDropdowns}
            showWeekNumbers={this.state.showWeekNumbers}
            locale={this.state.locale}
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            maxDate={this.state.maxDate}
            ranges={this.state.ranges}
            onEvent={this.handleEvent}
            pickedRange={this.props.reportType}
            rangeValue={this.state.rangeValue}
          >
            <Button className="selected-date-range-btn" style={buttonStyle}>
              <div className="pull-left">
                <i className="fa fa-calendar" />
                &nbsp;
                <span>{label}</span>
              </div>
              <div className="pull-right">
                <i className="fa fa-angle-down" />
              </div>
            </Button>
          </DatetimeRangePicker>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  reportType: state.moduleReportPicker.reportType,
  rangeNumber: state.moduleReportPicker.rangeNumber,
});

const mapDispatchToProps = {
  updateReportType,
  updateStartEndDate,
  updateStartEndDateCompare,
  numOfWeeks,
};

PickerDate = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PickerDate);

export default withStyles(styles)(PickerDate);
