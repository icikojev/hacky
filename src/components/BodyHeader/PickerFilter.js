import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux';
import { updateFilterList, resetFilters } from '../../redux/modules/clients';
import Filter from './Filter';
import _ from 'lodash';
import JsonFind from 'json-find';

const styles = () => ({
  formControl: {
    marginTop: 6,
    marginRight: 32,
    minWidth: 120,
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

class PickerFilter extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      reportFilter: '',
      open: false,
      filters: [],
      currentFilters: {},
      clients: [],
      chosenFilter: '',
      others: false,
    };
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      clients: nextProps.clients,
      network: nextProps.network,
      others: nextProps.others,
      selectedClient: nextProps.selectedClient,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.filters !== nextState.filters) {
      return true;
    }
    if (this.state.network !== nextState.network) {
      return true;
    }

    if (this.state.clients !== nextState.clients) {
      return true;
    }

    return false;
  }

  handleChange = (event, level) => {
    const chosenFilter = event.target.value;
    const { selectedClient, network, others } = this.state;
    const allFilters = others
      ? JsonFind(selectedClient.networks.others[network].filters)
      : JsonFind(selectedClient.networks[network].filters);
    const filters = allFilters.checkKey(chosenFilter);

    if (level === 0) {
      this.props.updateFilterList([chosenFilter]);
      this.setState({
        filters: [],
        currentFilters: {},
      });

      this.props.resetFilters();
    }

    if (Array.isArray(filters) && !_.isEmpty(filters)) {
      this.setState(previousState => {
        return {
          currentFilters: { ...previousState.currentFilters, [level]: chosenFilter },
          chosenFilter,
        };
      });

      if (!filters.includes('all')) {
        filters.unshift('all');
      }

      this.addFilter(filters);

      this.props.updateFilterList([
        ...Object.values({
          ...this.state.currentFilters,
          [level]: chosenFilter,
          [level + 1]: 'all',
        }),
      ]);
    } else if (_.isObject(filters) && !_.isEmpty(filters)) {
      this.setState(previousState => {
        return {
          currentFilters: { ...previousState.currentFilters, [level]: chosenFilter },
          chosenFilter,
        };
      });
      this.addFilter(Object.keys(filters));
      this.props.updateFilterList([
        ...Object.values({
          ...this.state.currentFilters,
          [level]: chosenFilter,
        }),
      ]);
    } else {
      if (level === 0) {
        this.props.updateFilterList([chosenFilter]);
      } else {
        this.setState(previousState => {
          return {
            currentFilters: { ...previousState.currentFilters, [level]: chosenFilter },
            chosenFilter,
          };
        });

        this.props.updateFilterList([
          ...Object.values({ ...this.state.currentFilters, [level]: chosenFilter }),
        ]);
      }
    }
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  addFilter = filter => {
    this.setState(previousState => {
      let hasFilter = false;

      _.forEach(previousState.filters, prevFilter => {
        if (_.isEqual(filter, prevFilter)) {
          hasFilter = true;
        }
      });

      if (hasFilter) {
        return {
          filters: [...previousState.filters],
        };
      }

      return {
        filters: [...previousState.filters, filter],
      };
    });
  };

  render() {
    const { classes } = this.props;
    const { network, filters, reportFilter, chosenFilter, selectedClient, others } = this.state;
    let { networks } = selectedClient;
    networks = others ? networks.others : networks;
    const clientFilters = network ? networks[network].filters : false;

    return (
      <form className={classes.flex} autoComplete="off">
        {!_.isEmpty(clientFilters) ? (
          <React.Fragment>
            <Filter
              level={0}
              classes={classes}
              onChange={this.handleChange}
              filters={Object.keys(clientFilters)}
            />
            {filters.map((filters, key) => (
              <Filter
                level={key + 1}
                key={`filters-${key}`}
                classes={classes}
                onChange={this.handleChange}
                filters={filters}
                chosenFilter={chosenFilter}
              />
            ))}
          </React.Fragment>
        ) : (
          <FormControl className={classes.formControl} disabled>
            <InputLabel htmlFor="controlled-open-select">Filter </InputLabel>
            <Select value={reportFilter} />
            <FormHelperText>Disabled</FormHelperText>
          </FormControl>
        )}
      </form>
    );
  }
}

const mapStateToProps = state => ({
  clients: state.clients.clients,
  network: state.clients.network,
  others: state.clients.others,
  selectedClient: state.clients.selectedClient,
});

const mapDispatchToProps = {
  updateFilterList,
  resetFilters,
};

PickerFilter = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PickerFilter);

export default withStyles(styles)(PickerFilter);
