import React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Icon from 'react-icons-kit';
import { ic_file_download } from 'react-icons-kit/md/ic_file_download';

const styles = theme => ({
  button: {
    background: theme.palette.secondary.main,
    marginTop: 18.5,
    marginRight: theme.spacing.unit * 4,
    width: 140,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

class LoadButton extends React.Component {
  handleClick = () => {
    this.props.getData(this.props.params);
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button
          className={classes.button}
          variant="raised"
          color="default"
          onClick={this.handleClick}
        >
          Load data
          <Icon icon={ic_file_download} className={classes.rightIcon} />
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(LoadButton);
