import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import DatetimeRangePicker from 'react-bootstrap-datetimerangepicker';
import moment from 'moment';
import 'bootstrap/dist/css/bootstrap.css';
import 'daterangepicker/daterangepicker.css';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import {
  updateReportType,
  updateStartEndDateCompare,
} from '../../redux/modules/moduleReportPicker';

const styles = theme => ({
  root: {},
});

class PickerDateCompare extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showDropdowns: true,
      showWeekNumbers: true,
      locale: {
        format: 'MM/DD/YYYY',
        separator: ' - ',
        applyLabel: 'Apply',
        cancelLabel: 'Cancel',
        fromLabel: 'From',
        toLabel: 'To',
        customRangeLabel: 'Custom',
        weekLabel: 'W',
        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        monthNames: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ],
        firstDay: 1,
      },
      pickedRange: 'period',
      rangeValue: 0,
      startDate: moment()
        .subtract(2, 'week')
        .startOf('isoWeek'),
      endDate: moment()
        .subtract(2, 'week')
        .endOf('isoWeek'),
      maxDate: moment()
        .subtract(2, 'week')
        .endOf('isoWeek'),
      opens: 'left',
      alwaysShowCalendars: 'true',
      ranges: {
        'Last Week': [
          moment()
            .subtract(1, 'week')
            .startOf('isoWeek'),
          moment()
            .subtract(1, 'week')
            .endOf('isoWeek'),
        ],
        'Last Month': [
          moment()
            .subtract(1, 'month')
            .startOf('month'),
          moment()
            .subtract(1, 'month')
            .endOf('month'),
        ],
      },
    };

    this.handleEvent = this.handleEvent.bind(this);
  }

  handleEvent(event, picker) {
    const { reportType, rangeNumber, updateStartEndDateCompare } = this.props;

    if (reportType === 'week') {
      this.setState({
        startDate: picker.startDate,
        endDate: picker.startDate
          .clone()
          .add(rangeNumber, 'week')
          .endOf('isoWeek'),
      });
    } else {
      this.setState({
        startDate: picker.startDate,
        endDate: picker.startDate
          .clone()
          .add(rangeNumber, 'month')
          .endOf('month'),
      });
    }

    updateStartEndDateCompare(picker.startDate.clone(), picker.endDate.clone());
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.reportType === 'week') {
      return {
        startDate: nextProps.startingDate
          .clone()
          .subtract(1, 'week')
          .startOf('isoWeek'),
        endDate: nextProps.endingDate
          .clone()
          .subtract(1, 'week')
          .endOf('isoWeek'),
      };
    } else {
      return {
        startDate: nextProps.startingDate
          .clone()
          .subtract(1, 'month')
          .startOf('month'),
        endDate: nextProps.endingDate
          .clone()
          .subtract(1, 'month')
          .endOf('month'),
      };
    }
  }

  render() {
    const start = this.state.startDate.format('ll');
    const end = this.state.endDate.format('ll');
    const label = start === end ? start : start + ' - ' + end;
    const buttonStyle = { width: '100%', minWidth: '240px' };

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          margin: '24px 16px 0px -80px',
          width: '320px',
        }}
        className="form-group"
      >
        <label
          style={{
            width: '120px',
            textAlign: 'center',
            padding: '0px',
          }}
          className="control-label col-md-3"
        >
          Compare:
        </label>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            padding: '0px',
            width: '240px',
          }}
          className="col-md-4"
        >
          <DatetimeRangePicker
            {...this.state}
            onEvent={this.handleEvent}
            pickedRange={this.props.reportType}
            rangeValue={this.props.rangeNumber}
          >
            <Button className="selected-date-range-btn" style={buttonStyle}>
              <div className="pull-left">
                <i className="fa fa-calendar" />
                &nbsp;
                <span>{label}</span>
              </div>
              <div className="pull-right">
                <i className="fa fa-angle-down" />
              </div>
            </Button>
          </DatetimeRangePicker>
        </div>
      </div>
    );
  }
}

PickerDateCompare.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  reportType: state.moduleReportPicker.reportType,
  startingDate: state.moduleReportPicker.startingDate,
  endingDate: state.moduleReportPicker.endingDate,
  rangeNumber: state.moduleReportPicker.rangeNumber,
});

const mapDispatchToProps = {
  updateReportType,
  updateStartEndDateCompare,
};

PickerDateCompare = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PickerDateCompare);

export default withStyles(styles)(PickerDateCompare);
