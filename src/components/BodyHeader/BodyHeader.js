import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PickerFilter from './PickerFilter';
import LoadButtonContainer from '../../containers/LoadButtonContainer';
import PickerDate from './PickerDate';
import PickerDateCompare from './PickerDateCompare';
import PickerReportContainer from '../../containers/PickerReportContainer';

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 0.3,
    marginLeft: theme.spacing.unit * 4,
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

class BodyHeader extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={`${classes.root} ${classes.flex}`}>
        <PickerReportContainer />
        <PickerDate />
        <PickerDateCompare />
        <div className={classes.flex}>
          <PickerFilter />
          <LoadButtonContainer />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(BodyHeader);
