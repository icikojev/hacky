import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from 'react-icons-kit';
import { googlePlus } from 'react-icons-kit/icomoon/googlePlus';

const SignInButton = props => {
  return (
    <div
      style={{
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '50vh',
      }}
    >
      <Typography
        style={{
          marginBottom: '20px',
        }}
        variant="title"
        gutterBottom
      >
        Peribian Reporting Tool
      </Typography>
      <Button
        variant="raised"
        style={{
          width: '160px',
          height: '60px',
          fontSize: '20px',
          textTransform: 'capitalize',
          background: '#dd4b39',
          color: '#FFFFFF',
        }}
        onClick={props.onClick}
      >
        <Icon icon={googlePlus} size={30} style={{ marginRight: '15px' }} />
        <div>Sign in</div>
      </Button>
    </div>
  );
};

export default SignInButton;
