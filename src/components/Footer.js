import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Icon from 'react-icons-kit';
import { facebook } from 'react-icons-kit/icomoon/facebook';
import { twitter } from 'react-icons-kit/icomoon/twitter';
import { instagram } from 'react-icons-kit/icomoon/instagram';
import { linkedin2 } from 'react-icons-kit/icomoon/linkedin2';

const styles = theme => ({
  root: {
    width: '100%',
    height: 40,
    backgroundColor: theme.palette.primary.dark,
    flex: 1,
    flexGrow: 1,
  },
  nav: {
    color: '#CFD8DC',
    height: 20,
    backgroundColor: theme.palette.primary.light,
    display: 'flex',
    alignItems: 'center',
  },
});

class Footer extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <div
        className={classes.root}
        style={{
          position: 'fixed',
          bottom: '0',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <div style={{ paddingTop: '4px', marginLeft: '20px' }}>
            <a
              href="https://hr-hr.facebook.com/seekandhit/"
              style={{
                fontWeight: '100',
                marginRight: '10px',
                color: '#fff',
              }}
            >
              <Icon size={24} icon={facebook} />
            </a>
            <a
              href="https://www.instagram.com/seekandhit/"
              style={{
                fontWeight: '100',
                marginRight: '10px',
                color: '#fff',
              }}
            >
              <Icon size={24} icon={instagram} />
            </a>
            <a
              href="https://twitter.com/seekandhit?lang=en"
              style={{
                fontWeight: '100',
                marginRight: '10px',
                color: '#fff',
              }}
            >
              <Icon size={24} icon={twitter} />
            </a>
            <a
              href="https://www.linkedin.com/company/seekandhit"
              style={{
                fontWeight: '100',
                marginRight: '10px',
                color: '#fff',
              }}
            >
              <Icon size={24} icon={linkedin2} />
            </a>
          </div>
        </div>
        <div
          className="footerText"
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            fontWeight: '100',
            color: '#CFD8DC',
            fontSize: '12px',
          }}
        >
          Copyright © 2007. - 2018. Uber d.o.o.
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              fontWeight: '100',
              marginRight: '10px',
              color: '#CFD8DC',
              fontSize: '12px',
            }}
          >
            Powered by
          </div>
          <div className="logo">
            <a href="https://seekandhit.com" style={{ height: '100px' }}>
              <img
                src="https://seekandhit.com/wp-content/themes/sih-2014/img/seekandhit.svg"
                alt="SeekandHit Internet Marketing"
              />
            </a>
          </div>
        </div>
      </div>
    );
  }
}
Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
