import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: theme.mixins.gutters({
    width: '100%',
    height: '50vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }),
});

const Message = props => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Typography variant="headline" component="h3">
        Please select client and network.
      </Typography>
      <Typography component="p">Molimo odaberite klijenta i mrežu.</Typography>
    </div>
  );
};

export default withStyles(styles)(Message);
