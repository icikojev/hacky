import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import capitalCase from 'to-capital-case';

const styles = theme => ({
  formControl: {
    minWidth: 200,
  },
});

class PickerBreakdown extends React.Component {
  state = {
    filter: '',
    open: false,
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.props.onChange(event.target.value);
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  render() {
    const { classes, dimensions } = this.props;

    return (
      <form
        style={{
          marginBottom: '30px',
        }}
        autoComplete="off"
      >
        {dimensions && (
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="controlled-open-select">Breakdown</InputLabel>
            <Select
              open={this.state.open}
              onClose={this.handleClose}
              onOpen={this.handleOpen}
              value={this.state.filter}
              onChange={this.handleChange}
              inputProps={{
                name: 'filter',
                id: 'controlled-open-select',
              }}
            >
              <MenuItem key="all" value="all">
                All
              </MenuItem>
              {dimensions.map(dimension => (
                <MenuItem key={dimension} value={dimension}>
                  {capitalCase(dimension)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
      </form>
    );
  }
}

export default withStyles(styles)(PickerBreakdown);
