import React from 'react';
import Icon from 'react-icons-kit';
import { arrowUp } from 'react-icons-kit/icomoon/arrowUp';
import { arrowDown } from 'react-icons-kit/icomoon/arrowDown';

const Arrow = props => (
  <Icon
    icon={props.down ? arrowDown : arrowUp}
    size={64}
    style={{ color: props.color, marginTop: '4px' }}
  />
);

export default Arrow;
