import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import PositionedSnackbar from './PositionedSnackbar';

const styles = theme => ({
  button: {
    background: theme.palette.secondary.main,
    marginTop: 12,
    marginRight: theme.spacing.unit * 4,
    marginLeft: theme.spacing.unit * 8,
    width: 140,
  },
  paperWidthMd: {
    maxWidth: 1200,
    minWidth: 1200,
  },
});

class ExportButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      openModal: props.openModal,
      exportLink: props.exportLink,
      copied: false,
      snackbarState: false,
    };
  }

  static getDerivedStateFromProps(props) {
    return {
      openModal: props.openModal,
      exportLink: props.exportLink,
    };
  }

  handleClickOpen = () => {
    const { exportType, exportData, getExportLink } = this.props;

    if (exportType === 'all') {
      getExportLink(exportData);
    } else {
      getExportLink({ ...exportData.breakdown[exportType], clientId: exportData.clientId });
    }

    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, snackbarState: false });
  };

  handleClickedClose = () => {
    this.setState({ open: false, snackbarState: true });
    setTimeout(() => {
      this.setState({ snackbarState: false });
    }, 1500);
  };

  render() {
    const { classes, dimensions } = this.props;

    return (
      <React.Fragment>
        {dimensions && (
          <React.Fragment>
            <Button
              className={classes.button}
              variant="raised"
              color="default"
              onClick={this.handleClickOpen}
              size="medium"
            >
              Export
            </Button>
            {this.state.snackbarState ? (
              <PositionedSnackbar snackbarState={this.state.snackbarState}>
                Copied.
              </PositionedSnackbar>
            ) : null}
            {this.props.openModal && (
              <Dialog
                maxWidth="md"
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
              >
                <DialogTitle id="form-dialog-title">Export link</DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    <a href={this.state.exportLink} target="_blank">
                      {this.state.exportLink}
                    </a>
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <CopyToClipboard
                    text={this.state.exportLink}
                    onCopy={() => this.setState({ copied: true })}
                  >
                    <Button onClick={this.handleClickedClose} color="primary">
                      Copy link to clipboard
                    </Button>
                  </CopyToClipboard>
                </DialogActions>
              </Dialog>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ExportButton);
