import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';

class PositionedSnackbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      vertical: 'top',
      horizontal: 'center',
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.snackbarState !== state.open) {
      return {
        open: props.snackbarState,
      };
    }
    return null;
  }
  handleClick = state => () => {
    this.setState({ open: true, ...state });
  };

  handleClose = () => {
    this.setState({ open: !this.props.snackbarState });
  };

  render() {
    const { vertical, horizontal, open } = this.state;
    return (
      <div>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">Link is copied to clipboard.</span>}
        />
      </div>
    );
  }
}

export default PositionedSnackbar;
