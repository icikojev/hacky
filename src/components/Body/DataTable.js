import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import _isEmpty from 'lodash/isEmpty';
import _isEqual from 'lodash/isEqual';
import Period from './Table/Period';
import Total from './Table/Total';
import Regular from './Table/Regular';
import TotalLabel from './Table/TotalLabel';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    height: 'max-content',
  },

  circularProgress: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
});

class DataTable extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      tableData: {},
      hiddenColumnNames: {},
      ...this.props,
    };
  }

  static getDerivedStateFromProps(nextProps) {
    return { ...nextProps };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.errorMessage !== nextProps.errorMessage) {
      return true;
    }

    if (this.state.errorMessage !== nextState.errorMessage) {
      return true;
    }

    if (!_isEqual(this.state.tableData, nextState.tableData)) {
      return true;
    }

    if (!_isEqual(this.state.hiddenColumnNames, nextState.hiddenColumnNames)) {
      return true;
    }

    return false;
  }

  hiddenColumnNamesChange = (hiddenColumn, metric) => {
    if (_isEmpty(hiddenColumn)) {
      const newHiddenColumnNames = {
        ...this.state.hiddenColumnNames,
        [metric]: { total: [], regular: [] },
      };
      this.props.updateHiddenColumnNames(newHiddenColumnNames);
    } else {
      const newHiddenColumnNames = {
        ...this.state.hiddenColumnNames,
        [metric]: {
          regular: hiddenColumn,
          total: hiddenColumn.map(name => {
            return `${name}_total`;
          }),
        },
      };
      this.props.updateHiddenColumnNames(newHiddenColumnNames);
    }
  };

  render() {
    const { tableData, hiddenColumnNames } = this.state;

    const PeriodTable = ({ period }) => <Period period={period} />;

    const TotalLabelTable = ({ totalLabel }) => <TotalLabel totalLabel={totalLabel} />;

    return (
      <React.Fragment>
        {!_isEmpty(tableData) && (
          <Paper
            style={{
              position: 'relative',
              width: '80vw',
              maxHeight: '60vh',
              overflow: 'auto',
            }}
            onScroll={this.handleScroll}
          >
            <div
              className="regular-tables"
              style={{
                display: 'flex',
                maxHeight: '100%',
              }}
            >
              <PeriodTable period={tableData.period} />
              {tableData.regular.map(item => (
                <Regular
                  key={`${item.metric}-regular`}
                  data={item}
                  hiddenColumnNames={hiddenColumnNames[item.metric].regular}
                  hiddenColumnNamesChange={this.hiddenColumnNamesChange}
                />
              ))}
            </div>
            <div
              className="total-tables"
              style={{
                display: 'flex',
                position: 'sticky',
                width: 'fit-content',
                bottom: '0px',
                background: '#FFFFFF',
                borderTop: '2px solid #BBB',
              }}
            >
              <TotalLabelTable totalLabel={tableData.totalLabel} />
              {tableData.total.map(item => (
                <Total
                  key={`${item.metric}-total`}
                  data={item}
                  hiddenColumnNames={hiddenColumnNames[item.metric].total}
                  hiddenColumnNamesChange={this.hiddenColumnNamesChange}
                />
              ))}
            </div>
          </Paper>
        )}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(DataTable);
