import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  ColumnChooser,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';
import { TableColumnVisibility } from './TableColumnVisibility';

class Regular extends Component {
  constructor(props) {
    super(props);

    this.state = { ...props };
  }

  static getDerivedStateFromProps(nextProps) {
    return { ...nextProps };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.hiddenColumnNames !== nextProps.hiddenColumnNames) {
      return true;
    }

    if (this.props.data !== nextProps.data) {
      return true;
    }

    if (this.state.data !== nextState.data) {
      return true;
    }

    if (this.state.hiddenColumnNames !== nextState.hiddenColumnNames) {
      return true;
    }

    return false;
  }

  render() {
    return (
      <div
        className="regular-table"
        style={{
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          width: '100%',
          height: '100%',
          borderLeft: '1px solid #e0e0e0',
        }}
      >
        <Typography
          variant="body2"
          style={{
            padding: '15px',
            borderBottom: '1px solid #e0e0e0',
            width: '100%',
            position: 'relative',
            fontSize: '0.7rem',
            fontWeight: '600',
          }}
          align="center"
        >
          {this.state.data.title}
        </Typography>
        <Grid rows={this.state.data.rows} columns={this.state.data.columns}>
          <Table columnExtensions={this.state.data.columnExtensions} />
          <TableHeaderRow />
          <TableColumnVisibility
            metric={this.state.data.metric}
            hiddenColumnNames={this.state.hiddenColumnNames}
            columnExtensions={this.state.data.visibilityColumnExtensions}
            onHiddenColumnNamesChange={this.props.hiddenColumnNamesChange}
          />
          <Toolbar />
          <ColumnChooser />
        </Grid>
      </div>
    );
  }
}

export default Regular;
