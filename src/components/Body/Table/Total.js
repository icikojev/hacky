import React, { Component } from 'react';
import '@devexpress/dx-react-grid';
import { Grid, Table } from '@devexpress/dx-react-grid-material-ui';
import { TableColumnVisibility } from './TableColumnVisibility';

class Total extends Component {
  constructor(props) {
    super(props);

    this.state = { ...props };
  }

  static getDerivedStateFromProps(nextProps) {
    return { ...nextProps };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.hiddenColumnNames !== nextProps.hiddenColumnNames) {
      return true;
    }

    if (this.state.hiddenColumnNames !== nextState.hiddenColumnNames) {
      return true;
    }

    if (this.state.data !== nextState.data) {
      return true;
    }

    return false;
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%',
          borderLeft: '1px solid #e0e0e0',
        }}
      >
        <Grid rows={this.state.data.rows} columns={this.state.data.columns}>
          <Table columnExtensions={this.state.data.columnExtensions} />
          <TableColumnVisibility
            metric={this.state.data.metric}
            hiddenColumnNames={this.state.hiddenColumnNames}
            columnExtensions={this.state.data.visibilityColumnExtensions}
            onHiddenColumnNamesChange={this.props.hiddenColumnNamesChange}
          />
        </Grid>
      </div>
    );
  }
}

export default Total;
