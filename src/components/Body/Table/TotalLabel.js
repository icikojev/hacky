import React, { PureComponent } from 'react';
import '@devexpress/dx-react-grid';
import { Grid, Table } from '@devexpress/dx-react-grid-material-ui';

class TotalLabel extends PureComponent {
  render() {
    return (
      <div
        key="totalLabel"
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          width: 'auto',
          borderLeft: '1px solid #e0e0e0',
        }}
      >
        <Grid rows={this.props.totalLabel.rows} columns={this.props.totalLabel.columns}>
          <Table columnExtensions={this.props.totalLabel.columnExtensions} />
        </Grid>
      </div>
    );
  }
}

export default TotalLabel;
