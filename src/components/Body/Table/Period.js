import React, { PureComponent } from 'react';
import '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';

class Period extends PureComponent {
  render() {
    return (
      <div
        key="period"
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          width: 'auto',
          borderLeft: '1px solid #e0e0e0',
        }}
      >
        <div
          style={{
            padding: '15px',
            width: '100%',
            minHeight: '50px',
            position: 'relative',
          }}
          align="center"
        />
        <Grid rows={this.props.period.rows} columns={this.props.period.columns}>
          <Table columnExtensions={this.props.period.columnExtensions} />
          <TableHeaderRow />
        </Grid>
      </div>
    );
  }
}

export default Period;
