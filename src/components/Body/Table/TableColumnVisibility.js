import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import {
  Action,
  Getter,
  Plugin,
  Template,
  TemplateConnector,
  TemplatePlaceholder,
  createStateHelper,
} from '@devexpress/dx-react-core';
import {
  toggleColumn,
  visibleTableColumns,
  tableDataColumnsExist,
  getColumnExtensionValueGetter,
} from '@devexpress/dx-grid-core';

const pluginDependencies = [{ name: 'Table' }];

const visibleTableColumnsComputed = ({ tableColumns, hiddenColumnNames }) =>
  visibleTableColumns(tableColumns, hiddenColumnNames);

const columnExtensionValueGetter = (columnExtensions, defaultValue) =>
  getColumnExtensionValueGetter(columnExtensions, 'togglingEnabled', defaultValue);

export class TableColumnVisibility extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hiddenColumnNames: props.hiddenColumnNames || props.defaultHiddenColumnNames,
      metric: props.metric || '',
    };
    const stateHelper = createStateHelper(this, {
      hiddenColumnNames: () => this.onHiddenColumnNamesChange,
    });

    this.toggleColumnVisibility = stateHelper.applyFieldReducer.bind(
      stateHelper,
      'hiddenColumnNames',
      toggleColumn,
    );

    this.onHiddenColumnNamesChange = this.onHiddenColumnNamesChange.bind(this);
  }

  onHiddenColumnNamesChange(hiddenColumnNames) {
    this.props.onHiddenColumnNamesChange(hiddenColumnNames, this.state.metric);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.hiddenColumnNames !== nextState.hiddenColumnNames) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    window.removeEventListener('hiddenColumnNames', this.onHiddenColumnNamesChange.bind(this));
  }

  static getDerivedStateFromProps(nextProps) {
    return { ...nextProps };
  }

  render() {
    const { hiddenColumnNames } = this.state;
    const { columnExtensions, columnTogglingEnabled } = this.props;

    return (
      <Plugin name="TableColumnVisibility" dependencies={pluginDependencies}>
        <Getter name="hiddenColumnNames" value={hiddenColumnNames} />
        <Getter name="tableColumns" computed={visibleTableColumnsComputed} />
        <Getter
          name="isColumnTogglingEnabled"
          value={columnExtensionValueGetter(columnExtensions, columnTogglingEnabled)}
        />
        <Action name="toggleColumnVisibility" action={this.toggleColumnVisibility} />

        <Template name="table">
          {params => (
            <TemplateConnector>
              {({ tableColumns }) =>
                tableDataColumnsExist(tableColumns) ? <TemplatePlaceholder /> : 'Nothing to show'
              }
            </TemplateConnector>
          )}
        </Template>
      </Plugin>
    );
  }
}

TableColumnVisibility.propTypes = {
  hiddenColumnNames: PropTypes.arrayOf(PropTypes.string),
  defaultHiddenColumnNames: PropTypes.arrayOf(PropTypes.string),
  onHiddenColumnNamesChange: PropTypes.func,
  messages: PropTypes.object,
  columnExtensions: PropTypes.array,
  columnTogglingEnabled: PropTypes.bool,
};

TableColumnVisibility.defaultProps = {
  hiddenColumnNames: undefined,
  defaultHiddenColumnNames: [],
  onHiddenColumnNamesChange: undefined,
  messages: {},
  columnExtensions: undefined,
  columnTogglingEnabled: true,
};
