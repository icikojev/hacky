import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Arrow from './Kpi/Arrow';
import styles from './Kpi/styles';

const DisplayArrow = props => {
  if (props.item.metric > props.item.value) {
    if (props.item.better === '>') {
      return <Arrow color="green" />;
    } else {
      return <Arrow color="red" />;
    }
  } else if (props.item.metric < props.item.value) {
    if (props.item.better === '>') {
      return <Arrow down={true} color="red" />;
    } else {
      return <Arrow down={true} color="green" />;
    }
  }
};

const Kpi = props => {
  const { classes, data } = props;
  var i = 0;

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: '16px',
        marginBottom: '24px',
      }}
    >
      {data.length > 0 ? (
        data.map(item => (
          <Paper key={i++} className={classes.root} elevation={1}>
            <div className={classes.upPart}>
              <div className={classes.upPartLeft}>
                <div className={classes.upPartLeft1}>{item.kpiName}</div>
                <div className={classes.upPartLeft2}>73%</div>
              </div>
              <DisplayArrow item={item} />
            </div>
            <div className={classes.downPart}>
              Target {item.better} {item.value} {item.metricName}
            </div>
          </Paper>
        ))
      ) : (
        <div />
      )}
    </div>
  );
};

export default withStyles(styles)(Kpi);
