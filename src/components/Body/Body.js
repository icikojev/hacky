import React from 'react';
import DataTable from './DataTable';
import _isEqual from 'lodash/isEqual';
import PickerBreakdown from './PickerBreakdown';
import ExportButtonContainer from '../../containers/ExportButtonContainer';
// import Kpi from './Kpi';

class Body extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...props };

    this.onPickerBreakDownChange = this.onPickerBreakDownChange.bind(this);
  }

  static getDerivedStateFromProps(props) {
    return { ...props };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!_isEqual(this.state.tableData, nextState.tableData)) {
      return true;
    }

    if (!_isEqual(this.props.tableData, nextProps.tableData)) {
      return true;
    }

    if (!_isEqual(this.state.hiddenColumnNames, nextState.hiddenColumnNames)) {
      return true;
    }

    if (!_isEqual(this.props.currentTable, nextProps.currentTable)) {
      return true;
    }

    if (!_isEqual(this.props.errorMessage, nextProps.errorMessage)) {
      return true;
    }

    return false;
  }

  onPickerBreakDownChange(currentTable) {
    this.props.updateCurrentTable(currentTable);
  }

  render() {
    const { client, network, tableData, errorMessage, others } = this.props;
    const currentTableData = tableData[this.state.currentTable];
    const dimensions =
      client.networks && network
        ? others
          ? client.networks.others[network].dimensions
          : client.networks[network].dimensions
        : false;
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
          padding: '10px 30px 0 32px',
          width: '100%',
        }}
      >
        {errorMessage ? (
          <div>{errorMessage}</div>
        ) : (
          <React.Fragment>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  marginBottom: '40px',
                  maxHeight: '50px',
                }}
              >
                <PickerBreakdown dimensions={dimensions} onChange={this.onPickerBreakDownChange} />
                <ExportButtonContainer
                  dimensions={dimensions}
                  exportType={this.state.currentTable}
                />
              </div>
              {/* <Kpi data={this.props.kpiData} /> */}
            </div>
            <DataTable {...this.props} tableData={currentTableData} />
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default Body;
