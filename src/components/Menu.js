import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import SelectClientContainer from '../containers/SelectClientContainer';
import { connect } from 'react-redux';
import { updateNetwork } from '../redux/modules/clients';
import { getData } from '../redux/modules/tableData';
import MenuItems from './Menu/Items';

const styles = theme => ({
  root: {
    maxWidth: 210,
    width: 210,
    minWidth: 210,
    backgroundColor: '#fff',
    color: theme.palette.primary.dark,
    marginTop: 40,
    minHeight: 700,
    height: 'calc(100vh - 80px)',
  },
  nested: {
    paddingLeft: theme.spacing.unit * 2,
  },
  subheader: {
    backgroundColor: '#B0BEC5',
    color: '#455A64',
    padding: 0,
    textAlign: 'center',
  },
});

class Menu extends React.Component {
  state = { open: false, network: '' };

  renderLink = itemProps => <Link to={this.props.to} {...itemProps} />;

  handleMainNetworkClick = network => () => {
    this.props.updateNetwork(network, false);
    this.getData(network, false);
    this.setState({ network });
  };

  handleOtherNetworkClick = network => () => {
    this.props.updateNetwork(network, true);
    this.getData(network, true);
    this.setState({ network });
  };

  getData = (network, others) => {
    if (this.props.params.filters.length > 0) {
      const { params } = this.props;
      const dimensions = others
        ? params.client.networks.others[network].dimensions
        : params.client.networks[network].dimensions;

      const configObj = {
        clientId: params.client.client_unique_id,
        network,
        dimensions,
        others,
        mainDateFrom: params.mainFrom.clone().format(),
        mainDateTo: params.mainTo.clone().format(),
        compareDateFrom: params.compareFrom.clone().format(),
        compareDateTo: params.compareTo.clone().format(),
        period: params.period,
        filters: params.filters,
      };

      this.props.getData(configObj);
    }
  };

  handleOpen = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { classes, selectedClient } = this.props;
    let networks = selectedClient.networks ? Object.keys(selectedClient.networks) : [];

    let otherNetworks = [];
    if (networks.includes('others')) {
      networks = networks.filter(item => item !== 'others');
      otherNetworks = Object.keys(selectedClient.networks.others);
    }

    return (
      <div className={classes.root}>
        <List
          style={{
            paddingBottom: '9px',
          }}
          component="nav"
          subheader={
            <ListSubheader className={classes.subheader} component="div">
              <SelectClientContainer />
            </ListSubheader>
          }
        >
          {selectedClient ? (
            <MenuItems
              selectedItem={this.state.network}
              items={networks}
              onClick={this.handleMainNetworkClick}
              style={{}}
            />
          ) : null}
          {otherNetworks.length > 0 && (
            <React.Fragment>
              <ListItem button onClick={this.handleOpen}>
                <ListItemText primary="Others" />
                {this.state.open ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <MenuItems
                    selectedItem={this.state.network}
                    items={otherNetworks}
                    onClick={this.handleOtherNetworkClick}
                  />
                </List>
              </Collapse>
            </React.Fragment>
          )}
        </List>
      </div>
    );
  }
}

Menu.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  clients: state.clients.clients,
  selectedClient: state.clients.selectedClient,
  params: {
    client: state.clients.selectedClient,
    filters: state.clients.selectedFilters,
    network: state.clients.network,
    period: state.moduleReportPicker.reportType,
    mainFrom: state.moduleReportPicker.startingDate,
    mainTo: state.moduleReportPicker.endingDate,
    compareFrom: state.moduleReportPicker.compareFrom,
    compareTo: state.moduleReportPicker.compareTo,
  },
});

const mapDispatchToProps = {
  updateNetwork,
  getData,
};

Menu = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Menu);

export default withStyles(styles)(Menu);
