import axios from 'axios';
// import Cookie from 'js-cookie';

const api = axios.create({
  baseURL: 'http://localhost:5000',
});

export default api;
