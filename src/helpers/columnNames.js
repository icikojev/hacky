export default {
  all: {
    title: 'All',
    format: 'plain',
    fill: 1,
  },
  week: {
    title: 'Week',
    format: 'plain',
    fill: 1,
  },
  month: {
    title: 'Month',
    format: 'plain',
    fill: 1,
  },
  gawTotalClicks: {
    title: 'Clicks',
    format: 'plain',
    fill: 1,
  },
  cos: {
    title: 'COS',
    format: 'percentage',
    fill: 1,
  },
  profit: {
    title: 'Profit',
    format: 'currency',
    fill: 1,
  },
  roi: {
    title: 'ROI',
    format: 'percentage',
    fill: 1,
  },
  totalCos: {
    title: 'Total COS',
    format: 'percentage',
    fill: 1,
  },
  totalProfit: {
    title: 'Total Profit',
    format: 'currency',
    fill: 1,
  },
  totalROI: {
    title: 'Total ROI',
    format: 'percentage',
    fill: 1,
  },
  totalBookings: {
    title: 'Total Bookings',
    format: 'plain',
    fill: 1,
  },
  totalBookingsValue: {
    title: 'Total Bookings Value',
    format: 'currency',
    fill: 1,
  },
  gawAvgCPC: {
    title: 'Avg. CPC',
    format: 'currency',
    fill: 0,
  },
  gawAvgPosition: {
    title: 'Avg. position',
    format: 'decimalOne',
    fill: 0,
  },
  gawAvgCtr: {
    title: 'CTR',
    format: 'percentage',
    fill: 1,
  },
  gawTotalCost: {
    title: 'Cost',
    format: 'currency',
    fill: 0,
  },
  gawAvgSIS: {
    title: 'Avg. IS',
    format: 'percentage',
    fill: 1,
  },
  gaSessions: {
    title: 'No of sessions',
    format: 'plain',
    fill: 1,
  },
  gaPercentNewSessions: {
    title: '% New sessions',
    format: 'percentage',
    fill: 1,
  },
  gaBounceRate: {
    title: 'Bounce rate',
    format: 'percentage',
    fill: 1,
  },
  gaPagesPerSession: {
    title: 'Pages per session',
    format: 'plain',
    fill: 1,
  },
  gaLastNonDirectBookingNum: {
    title: '_NETWORK_ Bookings',
    format: 'plain',
    fill: 1,
  },
  gaLastNonDirectBookingValue: {
    title: '_NETWORK_ Bookings Value',
    format: 'currency',
    fill: 1,
  },
  gaConversionRate: {
    title: 'Conversion rate',
    format: 'percentage',
    fill: 1,
  },
  mcfAssistedConversionsNumTotal: {
    title: 'Click Assisted Bookings',
    format: 'plain',
    fill: 1,
  },
  mcfAssistedConversionsValTotal: {
    title: 'Click Assisted Bookings Value',
    format: 'currency',
    fill: 1,
  },
  mcfAssistedConversionsNumLcTotal: {
    title: 'Direct Bookings',
    format: 'plain',
    fill: 1,
  },
  mcfAssistedConversionsValLcTotal: {
    title: 'Direct Bookings Value',
    format: 'currency',
    fill: 1,
  },
  gaReportingToolDecision: {
    title: 'Decision Goals - Total',
    format: 'plain',
    fill: 1,
  },
  gaReportingToolConsideration: {
    title: 'Consideration Goals - Total',
    format: 'plain',
    fill: 1,
  },
  gaReportingToolAwareness: {
    title: 'Awareness Goals - Total',
    format: 'plain',
    fill: 1,
  },
  mcfImpressionAndRichMediaConversionsNumTotal: {
    title: 'Assisted conversions - Impressions, Rich Media',
    format: 'plain',
    fill: 1,
  },
  mcfImpressionAndRichMediaConversionsValTotal: {
    title: 'Assisted conversions value - Impressions, Rich Media',
    format: 'currency',
    fill: 1,
  },
  fbActionsPostEngagement: {
    title: 'Actions Post Engagement',
    format: 'plain',
    fill: 1,
  },
  fbCpc: {
    title: 'Avg. CPC',
    format: 'currency',
    fill: 0,
  },
  fbSpend: {
    title: 'Cost',
    format: 'currency',
    fill: 0,
  },
  fbReach: {
    title: 'Reach',
    format: 'plain',
    fill: 1,
  },
  fbClicks: {
    title: 'Clicks',
    format: 'plain',
    fill: 1,
  },
  fbImpressions: {
    title: 'Impressions',
    format: 'plain',
    fill: 1,
  },
  fbTotalActions: {
    title: 'Actions',
    format: 'plain',
    fill: 1,
  },
  fbActionsLike: {
    title: 'New likes',
    format: 'plain',
    fill: 1,
  },
  bingTotalClicks: {
    title: 'Clicks',
    format: 'plain',
    fill: 1,
  },
  bingAvgPosition: {
    title: 'Avg. position',
    format: 'decimalOne',
    fill: 0,
  },
  bingTotalCost: {
    title: 'Cost',
    format: 'currency',
    fill: 0,
  },
  bingAvgSIS: {
    title: 'Avg. IS',
    format: 'percentage',
    fill: 1,
  },
  bingAvgCPC: {
    title: 'Avg. CPC',
    format: 'currency',
    fill: 0,
  },
  yanTotalClicks: {
    title: 'Clicks',
    format: 'plain',
    fill: 1,
  },
  yanAvgPosition: {
    title: 'Avg. position',
    format: 'decimalOne',
    fill: 0,
  },
  yanTotalCost: {
    title: 'Cost',
    format: 'currency',
    fill: 0,
  },
  yanAvgCPC: {
    title: 'Avg. CPC',
    format: 'currency',
    fill: 0,
  },
  yanAvgClickPosition: {
    title: 'Avg. Click Position',
    format: 'decimalOne',
    fill: 0,
  },
};
